<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<button type="button">Select All</button><br><br>

	<input type="checkbox" name="vehicle" value="Bike"> I have a bike<br>
	<input type="checkbox" name="vehicle" value="Car"> I have a car<br>
	<input type="checkbox" name="vehicle" value="Motorbike"> I have a Motorbike 

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('button').click(function(){
	            if($('input[type="checkbox"]').is(":checked")){
	                //alert("Checkbox is checked.");
	                $('button').html('Select All');
	                $('input[name=vehicle]').prop('checked', false);
	            }
	            else if($('input[type="checkbox"]').is(":not(:checked)")){
	                //alert("Checkbox is unchecked.");
	                $('button').html('Deselect All');
	                $('input[name=vehicle]').prop('checked', true);
	            }
	        });

			/*$('#check_all').click(function() {
			    $('input[name=vehicle]').prop('checked', true);
			});

			$('#uncheck_all').click(function() {
			    $('input[name=vehicle]').prop('checked', false);
			});*/
		});
	</script>
</body>
</html>
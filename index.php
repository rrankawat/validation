<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Apple</title>
</head>
<body>
	<form id="register-form">
		<div>
			<input type="text" name="email" placeholder="Email address">
		</div>
		<div>
			<input type="password" name="password" id="password" placeholder="Password">
		</div>
		<div>
			<input type="password" name="password2" placeholder="Re-enter password">
		</div>
		<div>
			<input type="text" name="firstName" placeholder="First Name">
		</div>
		<div>
			<input type="text" name="lastName" placeholder="Last Name">
		</div>
		<input type="submit" id="submit-button" class="btn btn-primary" value="Sign Up">
	</form>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
	<script src="validation.js"></script>
</body>
</html>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <title>Hello, world!</title>
</head>
<body>
    <div class="container">
      <br/>
      <form id="register-form">
        <div class="form-group">
          <label>Email address</label>
          <input type="email" name="email" class="form-control" placeholder="Enter email">
          <label id="email-error" class="error invalid-feedback" for="email"></label>
        </div>
        <div class="form-group">
          <label>Password</label>
          <input type="password" name="password" class="form-control" id="password" placeholder="Password">
          <label id="password-error" class="error invalid-feedback" for="password"></label>
        </div>
        <div class="form-group">
          <label>Re-Enter Password</label>
          <input type="password" name="password2" class="form-control" placeholder="Password">
          <label id="password2-error" class="error invalid-feedback" for="password2"></label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
      </form>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/additional-methods.min.js"></script>
    <script src="server.js"></script>
</body>
</html>